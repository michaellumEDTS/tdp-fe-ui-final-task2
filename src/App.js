import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div>
        <div>
          <a href="/#"><b>EDTS</b> TDP Batch #2</a>
          <a href="/#">Projects</a>
          <a href="/#">About</a>
          <a href="/#">Contact</a>
        </div>
      </div>

      {/* Header */}
      <header>
        <img src={banner} alt="Architecture" width={1500} height={800} />
        <div>
          <h1>
              EDTS Architects
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className="container">

        {/* Project Section */}
        <div id="projects">
          <h3>Projects</h3>
        </div>
        
        <div >Summer House</div>
        <img src={house2} alt="House" />
        <div>Renovated</div>
        <img src={house4} alt="House" />
        <div >Barn House</div>
        <img src={house5} alt="House" />
        <div>Summer House</div>
        <img src={house3} alt="House" />
        <div>Brick House</div>
        <img src={house2} alt="House" />
        <div>Renovated</div>
        <img src={house5} alt="House" />
        <div>Barn House</div>
        <img src={house4} alt="House" />

        {/* About Section */}
        <div>
          <h3>About</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div>
          <div >
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p>CEO &amp; Founder</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div >
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div >
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
          <div >
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button>Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}
        <div id="contact">
          <h3>Contact</h3>
          <p>Lets get in touch and talk about your next project.</p>

          {/***  
           add your element form here 
           ***/}

        </div>

        {/* Image of location/map */}
        <div>
          <img src={map} alt="maps" />
        </div>

        <div>
          <h3>Contact List</h3>

          {/***  
           add your element table here 
           ***/}

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
